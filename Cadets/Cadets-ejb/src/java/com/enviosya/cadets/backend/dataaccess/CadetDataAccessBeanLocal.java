package com.enviosya.cadets.backend.dataaccess;

import com.enviosya.cadets.backend.entity.Cadet;
import java.util.List;
//import backend.exceptions.DuplicateUserException;
//import backend.exceptions.UserNotFoundException;

import javax.ejb.Local;

@Local
public interface CadetDataAccessBeanLocal {
    void createCadet(Cadet client); //throws DuplicateUserException;
    
    Cadet getCadet(Long id); //throws UserNotFoundException;

    void editCadet(Cadet cadet); //throws UserNotFoundException;
   
    boolean cadetExists(Long id);
    
    List<Cadet> findRange();
}
