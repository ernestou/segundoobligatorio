package com.enviosya.cadets.backend.dataaccess;

import com.enviosya.cadets.backend.entity.Vehicle;
//import backend.exceptions.DuplicateVehicleException;
//import backend.exceptions.VehicleNotFoundException;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class VehicleDataAccessBean extends AbstractFacade<Vehicle> implements VehicleDataAccessBeanLocal {

    @PersistenceContext(unitName = "Cadets-ejbPU")
    private EntityManager em;
    
    public VehicleDataAccessBean() {
        super(Vehicle.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public boolean vehicleExists(Long id) {
        return find(id) != null;
    }
    
    @Override
    public Vehicle getVehicle(Long id) {
        Vehicle vehicle = find(id);
        if (vehicle == null) { 
            //throw new VehicleNotFoundException(); 
        }
        return vehicle;
    }

    @Override
    public void createVehicle(Vehicle vehicle) {
        create(vehicle);
    }
    
   
}
