package com.enviosya.cadets.backend.operations;

import com.enviosya.cadets.backend.entity.Vehicle;
import javax.ejb.Local;

@Local
public interface VehicleBeanLocal {
    void createVehicle(Vehicle vehicle); //throws DuplicateUserException;
    
    Vehicle getVehicle (Long id); //throws UserNotFoundException;
    
    boolean vehicleExists(Long id);    
}
