package com.enviosya.cadets.backend.dataaccess;

import com.enviosya.cadets.backend.entity.Cadet;
//import backend.exceptions.DuplicateCadetException;
//import backend.exceptions.CadetNotFoundException;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class CadetDataAccessBean extends AbstractFacade<Cadet> implements CadetDataAccessBeanLocal {

    @PersistenceContext(unitName = "Cadets-ejbPU")
    private EntityManager em;
    
    public CadetDataAccessBean() {
        super(Cadet.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public boolean cadetExists(Long id) {
        return find(id) != null;
    }
    
    @Override
    public Cadet getCadet(Long id) {
        Cadet cadet = find(id);
        if (cadet == null) { 
            //throw new CadetNotFoundException(); 
        }
        return cadet;
    }

    @Override
    public void createCadet(Cadet cadet) {
        create(cadet);
    }
    
    @Override
    public void editCadet(Cadet cadet) {
        edit(cadet);
    }
    
    @Override
    public List<Cadet> findRange() {
        int[] range = {0, 3};
        
        return findRange(range);
    }
}
