package com.enviosya.cadets.backend.operations;

import com.enviosya.cadets.backend.dataaccess.CadetDataAccessBeanLocal;
import com.enviosya.cadets.backend.entity.Cadet;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;


@Stateless
public class CadetBean implements CadetBeanLocal {

    @EJB
    private CadetDataAccessBeanLocal cadetFacade;    
    
    @Override
    public void createCadet(Cadet cadet) {
        cadetFacade.createCadet(cadet);
    }
    
    @Override
    public Cadet getCadet(Long id) {
        return cadetFacade.getCadet(id);
    }

    @Override
    public void editCadet(Cadet cadet) {
        cadetFacade.editCadet(cadet);
    }
   
    @Override
    public boolean cadetExists(Long id) {
        return cadetFacade.cadetExists(id);
    }
    
    @Override
    public List<Cadet> findClosedCadets() {        
        return cadetFacade.findRange();
    }   
}
