package com.enviosya.cadets.backend.operations;

import com.enviosya.cadets.backend.dataaccess.VehicleDataAccessBeanLocal;
import com.enviosya.cadets.backend.entity.Vehicle;
import javax.ejb.EJB;
import javax.ejb.Stateless;


@Stateless
public class VehicleBean implements VehicleBeanLocal {

    @EJB
    private VehicleDataAccessBeanLocal vehicleService;
    
    @Override
    public void createVehicle(Vehicle vehicle) {
        vehicleService.createVehicle(vehicle);
    }
    
    @Override
    public Vehicle getVehicle (Long id) {
        return vehicleService.getVehicle(id);
    }
    
    @Override
    public boolean vehicleExists(Long id) {
        return vehicleService.vehicleExists(id);
    }
}
