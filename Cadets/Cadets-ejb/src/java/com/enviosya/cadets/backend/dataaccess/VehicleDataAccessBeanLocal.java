package com.enviosya.cadets.backend.dataaccess;

import com.enviosya.cadets.backend.entity.Vehicle;
//import backend.exceptions.DuplicateUserException;
//import backend.exceptions.UserNotFoundException;

import javax.ejb.Local;

@Local
public interface VehicleDataAccessBeanLocal {
  
    void createVehicle(Vehicle vehicle); //throws DuplicateUserException;
    
    Vehicle getVehicle (Long id); //throws UserNotFoundException;
    
    boolean vehicleExists(Long id);
}
