package com.enviosya.cadets.backend.operations;

import com.enviosya.cadets.backend.entity.Cadet;
import java.util.List;
import javax.ejb.Local;


@Local
public interface CadetBeanLocal {
    void createCadet(Cadet cadet);
    
    Cadet getCadet(Long id); //throws UserNotFoundException;

    void editCadet(Cadet cadet); //throws UserNotFoundException;
   
    boolean cadetExists(Long id);
    
    List<Cadet> findClosedCadets();    
}
