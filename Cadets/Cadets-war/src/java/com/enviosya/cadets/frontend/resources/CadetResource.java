package com.enviosya.cadets.frontend.resources;

import com.enviosya.cadets.backend.operations.CadetBeanLocal;
import com.enviosya.cadets.backend.entity.Cadet;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import com.google.gson.Gson;
import com.enviosya.cadets.backend.entity.Vehicle;
import com.enviosya.cadets.backend.operations.VehicleBeanLocal;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.Iterator;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author riccager
 */
@Path("cadet")
@RequestScoped
public class CadetResource {

    @Context
    private UriInfo context;
    
    @Inject
    private CadetBeanLocal cadetService;
    
    @Inject
    private VehicleBeanLocal vehicleService;
    
    
    /**
     * Creates a new instance of CadetResource
     */
    public CadetResource() {
    }
    
    @POST
    @Path("create-cadet")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    public String createCadet(String jsonData) {
        try {
            Gson jsonParser = new Gson();
            Cadet newCadet = jsonParser.fromJson(jsonData, Cadet.class);

            cadetService.createCadet(newCadet);
            //Log4j.LOGGER.info("Cadete Agregado");
            return "Cadete agregado correctamente.";
        } catch (Exception e) {
            //Log4j.LOGGER.info("Error al insertar revisar gramatica Json " + e.getMessage());
            return "Error al insertar revisar gramatica Json " + e.getMessage();
        }
    }
    
    @POST
    @Path("add-vehicle")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    public String addVehicle(String jsonData) {
        try {
            Gson jsonParser = new Gson();
            JsonParser parser = new JsonParser();
            JsonObject data = parser.parse(jsonData).getAsJsonObject();
            
            Long cadetId = jsonParser.fromJson(data.get("cadet"), Long.class);
            
            Vehicle newVehicle = jsonParser.fromJson(data.get("vehicle"), Vehicle.class);
            
            Cadet cadet = cadetService.getCadet(cadetId);
            
            newVehicle.setCadet(cadet);
            cadet.getVehicles().add(newVehicle);
            
            vehicleService.createVehicle(newVehicle);
            cadetService.editCadet(cadet);

            //usuarioBean.agregarUsuario(nuevoCadete);
            //Log4j.LOGGER.info("Cadete Agregado");
            return "Vehiculo agregado correctamente.";
        } catch (Exception e) {
            //Log4j.LOGGER.info("Error al insertar revisar gramatica Json " + e.getMessage());
            return "Error al insertar revisar gramatica Json " + e.getMessage();
        }
    }
    
    @GET
    @Path("get-close-cadets")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCloseCadets() {
        List<Cadet> closeCadets = cadetService.findClosedCadets();
        String json = "{ cadets: [";
        
        for (Iterator<Cadet> iterator = closeCadets.iterator(); iterator.hasNext();) {
            Cadet next = iterator.next();
            
            if (iterator.hasNext()) {
                json += next.getId().toString() + ",";
            } else {
                json += next.getId().toString();
            }
        }
        
        json += "] }";
        
        return json;
    }
    
    @GET
    @Path("/get-cadet-info")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCadetInfo(@QueryParam("cadetId") Long cadetId) {
        Cadet currentCadet = cadetService.getCadet(cadetId);
        
        Vehicle cadetVehicle = currentCadet.getVehicles().get(0);
        
        return "{ vehicle: " + cadetVehicle.getId().toString() + ", "
               + "email: \"" + currentCadet.getEmail() + "\","
               + "fullname: \"" + currentCadet.getNombreCompleto() + "\" }";
    }
}