package com.enviosya.reviews.backend.operations;

import com.enviosya.reviews.backend.entities.Review;
import java.util.List;
import javax.ejb.Local;

@Local
public interface ReviewBeanLocal {
    
    void createReview(Review review);
    
    List<Review> getReviewsByShipmentAndAuthor(int shipmentId, int clientId);
    
    List<Review> getReviewsByCadetAndStatus(int cadetId, String status);
    
    void updateFeelingReview(Long reviewId, String feeling);
}
