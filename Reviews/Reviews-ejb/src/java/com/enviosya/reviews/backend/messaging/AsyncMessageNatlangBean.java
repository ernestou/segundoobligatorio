package com.enviosya.reviews.backend.messaging;

import com.enviosya.entitydto.dtos.ReviewDto;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

@Stateless
public class AsyncMessageNatlangBean implements AsyncMessageNatlangBeanLocal {

    @Resource(lookup = "jms/ConnectionFactory")
    private ConnectionFactory cf;
    
    @Resource(lookup = "jms/QueueNatlang")
    private Queue queueNatlang; 
    
    public void enqueue(Object message) {
        try {
            Connection connection = cf.createConnection();
            Session session = connection.createSession();
            ObjectMessage objectMessage = session.createObjectMessage();
            
            objectMessage.setObject((ReviewDto) message);
            
            connection.start();
            
            MessageProducer producer = session.createProducer(this.queueNatlang);
            producer.send(objectMessage);
            
            session.close();
            connection.close();
        } catch (JMSException ex) {
            System.out.println(ex.getMessage());
            //Logger.getLogger(UsuarioBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
