package com.enviosya.reviews.backend.dataaccess;

import com.enviosya.reviews.backend.entities.Review;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;


@Stateless
public class ReviewDataAccessBean extends AbstractFacade<Review> implements ReviewDataAccessBeanLocal {
    
    @PersistenceContext(unitName = "Reviews-ejbPU")
    private EntityManager em;
    
    public ReviewDataAccessBean() {
        super(Review.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }      
    
    @Override
    public void createReview(Review review){
        create(review);
    }
    
    @Override
    public Review getReview(Long id){
        Review review = find(id);
        if (review == null) { 
            //throw new NotFoundException(); 
        }
        return review;        
    }

    @Override
    public void editReview(Review review){
        edit(review);
    }
   
    @Override
    public boolean reviewExists(Long id){
        return find(id) != null;
    }
    
    @Override 
    public List<Review> getReviewsByShipmentAndAuthor(int shipmentId, int clientAuthorId) {
        TypedQuery<Review> query = em.createQuery("SELECT r "
                                                + "FROM Review r "
                                                + "WHERE r.shipmentId = :shipment "
                                                + "AND r.clientIdAuthor = :author", Review.class);
        query.setParameter("shipment", shipmentId);
        query.setParameter("author", clientAuthorId);
        List<Review> results = query.getResultList();
        return results;
    }
    
    @Override 
    public List<Review> getReviewsByCadetAndStatus(int cadetId, String status) {
        TypedQuery<Review> query = em.createQuery("SELECT r "
                                                + "FROM Review r "
                                                + "WHERE r.cadetId = :cadet "
                                                + "AND r.status = :status "
                                                + "ORDER BY r.id DESC", Review.class);
        query.setParameter("cadet", cadetId);
        query.setParameter("status", status);
        List<Review> results = query.getResultList();
        return results;
    }      
}
