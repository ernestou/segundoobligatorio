package com.enviosya.reviews.backend.dataaccess;

import com.enviosya.reviews.backend.entities.Review;
import java.util.List;
import javax.ejb.Local;

@Local
public interface ReviewDataAccessBeanLocal {
    void createReview(Review review);
    
    Review getReview(Long id);

    void editReview(Review review);
   
    boolean reviewExists(Long id);
    
    List<Review> getReviewsByShipmentAndAuthor(int shipmentId, int clientAuthorId);
    
    List<Review> getReviewsByCadetAndStatus(int cadetId, String status);
}
