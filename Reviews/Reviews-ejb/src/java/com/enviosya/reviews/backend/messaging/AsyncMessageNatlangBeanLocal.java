package com.enviosya.reviews.backend.messaging;

import javax.ejb.Local;

@Local
public interface AsyncMessageNatlangBeanLocal {
    void enqueue(Object message);
}
