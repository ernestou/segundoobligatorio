package com.enviosya.reviews.backend.operations;

import com.enviosya.reviews.backend.dataaccess.ReviewDataAccessBeanLocal;
import com.enviosya.reviews.backend.entities.Review;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class ReviewBean implements ReviewBeanLocal {

    @EJB
    private ReviewDataAccessBeanLocal reviewFacade;
    
    @Override
    public void createReview(Review review) {
        reviewFacade.createReview(review);
    }
    
    public List<Review> getReviewsByShipmentAndAuthor(int shipmentId, int clientId) {
        return reviewFacade.getReviewsByShipmentAndAuthor(shipmentId, clientId);
    }
    
    public List<Review> getReviewsByCadetAndStatus(int cadetId, String status) {
        return reviewFacade.getReviewsByCadetAndStatus(cadetId, status);
    }    
    
    public void updateFeelingReview(Long reviewId, String feeling) {
        Review currentReview = reviewFacade.getReview(reviewId);
        
        currentReview.setFeeling(feeling);
        
        if (currentReview.getStatus() == "Pending") {
            currentReview.setStatus("Approved");
        }
        
        reviewFacade.editReview(currentReview);
    }
}
