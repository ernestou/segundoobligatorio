package com.enviosya.reviews.backend.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "review")
public class Review {
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;    
    
    private int ratingService;
    private String commentService;
    private int ratingCadet;
    private String commentCadet;
    
    private int shipmentId;
    
    private int cadetId;
    
    private int clientIdAuthor;
    
    private String feeling;
    
    private String status;

    public Review() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public int getRatingService() {
        return ratingService;
    }

    public void setRatingService(int ratingService) {
        this.ratingService = ratingService;
    }

    public String getCommentService() {
        return commentService;
    }

    public void setCommentService(String commentService) {
        this.commentService = commentService;
    }

    public int getRatingCadet() {
        return ratingCadet;
    }

    public void setRatingCadet(int ratingCadet) {
        this.ratingCadet = ratingCadet;
    }

    public String getCommentCadet() {
        return commentCadet;
    }

    public void setCommentCadet(String commentCadet) {
        this.commentCadet = commentCadet;
    }

    public int getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(int shipmentId) {
        this.shipmentId = shipmentId;
    }

    public int getCadetId() {
        return cadetId;
    }

    public void setCadetId(int cadetId) {
        this.cadetId = cadetId;
    }

    public int getClientIdAuthor() {
        return clientIdAuthor;
    }

    public void setClientIdAuthor(int clientIdAuthor) {
        this.clientIdAuthor = clientIdAuthor;
    }

    public String getFeeling() {
        return feeling;
    }

    public void setFeeling(String feeling) {
        this.feeling = feeling;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }    
}
