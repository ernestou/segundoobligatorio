/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviosya.reviews.frontend.restclients;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

/**
 * Jersey REST client generated for REST resource:ShipmentResource
 * [shipment]<br>
 * USAGE:
 * <pre>
        ShipmentRestClientt client = new ShipmentRestClientt();
        Object response = client.XXX(...);
        // do whatever with response
        client.close();
 </pre>
 *
 * @author ernestou
 */
public class ShipmentRestClientt {

    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://localhost:8080/Shipments-war";

    public ShipmentRestClientt() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("shipment");
    }

    public String getShipmentParticipants(String shipmentId) throws ClientErrorException {
        WebTarget resource = webTarget;
        if (shipmentId != null) {
            resource = resource.queryParam("shipmentId", shipmentId);
        }
        resource = resource.path("get-shipment-participants");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(String.class);
    }

    public void close() {
        client.close();
    }
    
}
