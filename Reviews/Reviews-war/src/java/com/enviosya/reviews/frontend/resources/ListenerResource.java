package com.enviosya.reviews.frontend.resources;

import com.enviosya.reviews.backend.operations.ReviewBeanLocal;
import com.enviosya.entitydto.dtos.ReviewDto;
import com.enviosya.mocknatlang.restclients.NatlangRestClientInterface;
import com.enviosya.mocknatlang.restclients.NatlangRestClient;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/QueueNatlang"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
    })

@Path("")
public class ListenerResource implements MessageListener {

    @Context
    private UriInfo context;

    @Inject
    private ReviewBeanLocal reviewService;
    
    private NatlangRestClientInterface natlangService;

    public ListenerResource() {
        this.natlangService = new NatlangRestClient();
    }

    @Override
    public void onMessage(Message message) {
        try {
            ObjectMessage objMessage = (ObjectMessage) message;
            
            ReviewDto reviewDto = (ReviewDto) objMessage.getObject();
            
            String responseFeeling = this.natlangService.getFeelingNatlang(reviewDto.getComment());
            
            reviewService.updateFeelingReview(reviewDto.getReviewId(), responseFeeling);
            
            // MANDAR MAIL A CADETE
        } catch (JMSException ex) {
            Logger.getLogger(ListenerResource.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
