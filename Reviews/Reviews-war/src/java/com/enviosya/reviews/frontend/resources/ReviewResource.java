package com.enviosya.reviews.frontend.resources;

import com.enviosya.reviews.backend.entities.Review;
import com.enviosya.reviews.backend.operations.ReviewBeanLocal;
import com.enviosya.reviews.backend.messaging.AsyncMessageNatlangBeanLocal;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.enviosya.entitydto.dtos.ReviewDto;
import com.enviosya.reviews.frontend.restclients.ShipmentRestClientt;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("review")
@RequestScoped
public class ReviewResource {

    @Context
    private UriInfo context;

    @Inject
    private ReviewBeanLocal reviewService;

    @Inject
    private AsyncMessageNatlangBeanLocal asyncMessageNatlangService;    
    
    public ReviewResource() {
    }

    @POST
    @Path("create-review")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String createReview(String jsonData) throws FileNotFoundException {
        try {            
//            https://accounts.google.com/o/oauth2/v2/auth?
// client_id=392753515061-etpcdbb897l4vr2f9i4p2bm71gp8m14s.apps.googleusercontent.com&
// response_type=code&
// scope=openid%20email&
// redirect_uri=https://oauth2-login-demo.example.com/code&
// state=[state]
            
            Gson jsonParser = new Gson();
            JsonParser parser = new JsonParser();
            Review newReview = jsonParser.fromJson(jsonData, Review.class);
            newReview.setStatus("Pending");
            
            int shipmentId = newReview.getShipmentId();
            int clientIdAuthor = newReview.getClientIdAuthor();
                     
            // 1 - Validar usuario autor esté logueado y sea el mismo que solicitó el envío.
            ShipmentRestClientt shipmentRestClient = new ShipmentRestClientt();
            
            String jsonDataShipments = shipmentRestClient.getShipmentParticipants(String.valueOf(shipmentId));
            JsonObject dataShipment = parser.parse(jsonDataShipments).getAsJsonObject();

            int shipmentClientFromId = jsonParser.fromJson(dataShipment.get("clientFromId"), int.class);
            int shipmentClientToId = jsonParser.fromJson(dataShipment.get("clientToId"), int.class);
            int shipmentCadetId = jsonParser.fromJson(dataShipment.get("cadetId"), int.class);            
            
            shipmentRestClient.close();
            
            if (!(clientIdAuthor == shipmentClientFromId || clientIdAuthor == shipmentClientToId)) {
                return "ERROR - PASO 1";
            }
            
            // 2 - Validar que no exista un review para ese envio y por ese cliente.
            List<Review> reviewsShipmentAuthor;
            reviewsShipmentAuthor = reviewService.getReviewsByShipmentAndAuthor(shipmentId, clientIdAuthor);
            if (reviewsShipmentAuthor.size() > 0) {
                return "ERROR - PASO 2";
            }
            
            // 3 - Validar que comentarios sean de largo X
            if (newReview.getCommentCadet().length() > 200 || newReview.getCommentService().length() > 200) {
                return "ERROR - PASO 3";
            
            }
            
            // 4 - Validar que comentario no contenga "malas palabras"
            //JsonElement jsonElement = parser.parse(new FileReader("/Users/ernestou/Documents/Arq. sistemas/SegundoObligatorio/Reviews/Reviews-war/src/java/blacklist.json"));
            JsonElement jsonElement = parser.parse(ReviewResource.class.getResource("/blacklist.json").getPath());
            JsonArray jsonArray = jsonElement.getAsJsonArray();

            for (int i = 0; i < jsonArray.size(); i++) {
                String word = jsonArray.get(i).getAsString();
                if (newReview.getCommentCadet().contains(word) || newReview.getCommentService().contains(word)) {
                    newReview.setStatus("Rejected");
                }
            }            
            
            reviewService.createReview(newReview);
            
            // 5 - Natlang
            ReviewDto reviewDto = new ReviewDto();
            reviewDto.setReviewId(newReview.getId());
            reviewDto.setComment(newReview.getCommentCadet() + " " + newReview.getCommentService());
            asyncMessageNatlangService.enqueue(reviewDto);            
            
            return "Review creado correctamente";
            
        } catch (JsonSyntaxException | ClientErrorException e) {
            return "ERROR: " + e.getMessage();
        }
//        } catch (IOException ex) {
//            Logger.getLogger(ReviewResource.class.getName()).log(Level.SEVERE, null, ex);
//            return "ERROR: " + ex.getMessage();
//        }
    }
            
    @GET
    @Path("/get-cadet-reviews")
    @Produces(MediaType.APPLICATION_JSON)
    public String getReviewsByCadet(@QueryParam("cadetId") int cadetId) {
        List<Review> reviewsCadet;
        reviewsCadet = reviewService.getReviewsByCadetAndStatus(cadetId, "Approved");

        String jsonReturn = "{ cadets: [";
        
        System.err.println(reviewsCadet.size());
        System.err.println(reviewsCadet.get(0).getCommentService());
        
        for (Iterator<Review> iterator = reviewsCadet.iterator(); iterator.hasNext();) {
            Review next = iterator.next();
            
            jsonReturn += "{ ";
            
            jsonReturn += " ratingService: " + next.getRatingService() + ",";
            jsonReturn += " commentService: " + next.getCommentService() + ",";
            jsonReturn += " ratingCadet: " + next.getRatingCadet() + ",";
            jsonReturn += " commentCadet: " + next.getCommentCadet() + ",";
            jsonReturn += " clientIdAuthor: " + next.getClientIdAuthor() + ",";
            jsonReturn += " feeling: " + next.getFeeling();
            
            if (iterator.hasNext()) {
                jsonReturn += "},";
            } else {
                jsonReturn += "}";
            }
        }       
        
        jsonReturn += "] }";
        
        return jsonReturn;
    }            
    
}
