package com.enviosya.shipments.frontend.resources;

import com.enviosya.shipments.backend.entity.Shipment;
import com.enviosya.shipments.backend.operations.ShipmentBeanLocal;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.enviosya.costcalculator.operations.CostCalculator;
import com.enviosya.costcalculator.operations.CostCalculatorInterface;
import com.enviosya.shipments.frontend.restclients.CadetsRestClient;
import com.enviosya.shipments.frontend.restclients.ClientsRestClient;
import javax.ws.rs.GET;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author riccager
 */
@Path("shipment")
@RequestScoped
public class ShipmentResource {

    @Context
    private UriInfo context;
    
    @Inject
    private ShipmentBeanLocal shipmentService;
    
    private CostCalculatorInterface costCalcService;
    
    /**
     * Creates a new instance of ShipmentResource
     */
    public ShipmentResource() {
        this.costCalcService = new CostCalculator();
    }
    
    @POST
    @Path("create-shipment")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String createShipment(String jsonData) {
        try {
            Gson jsonParser = new Gson();
            JsonParser parser = new JsonParser();
            
            JsonObject data = parser.parse(jsonData).getAsJsonObject();
            
            String imageBase64 = jsonParser.fromJson(data.get("image"), String.class);          
            
            Shipment newShipment = jsonParser.fromJson(data.get("shipment"), Shipment.class);
            newShipment.setStatus("Pending");
            newShipment.setCost(costCalcService.obtainCost(imageBase64));
            
            ClientsRestClient clientsRestClient = new ClientsRestClient();
            String jsonDataClientFrom = clientsRestClient.getClientInfo(String.valueOf(newShipment.getClientFromId()));
            String jsonDataClientTo = clientsRestClient.getClientInfo(String.valueOf(newShipment.getClientToId()));
            JsonObject dataClientFrom = parser.parse(jsonDataClientFrom).getAsJsonObject();
            JsonObject dataClientTo = parser.parse(jsonDataClientTo).getAsJsonObject();
            
            newShipment.setClientFromName(jsonParser.fromJson(dataClientFrom.get("fullname"), String.class));
            newShipment.setClientToName(jsonParser.fromJson(dataClientTo.get("fullname"), String.class));           

            shipmentService.createShipment(newShipment);
            
            CadetsRestClient cadetsRestClient = new CadetsRestClient();
            
            return cadetsRestClient.getCloseCadets();
            
            //Log4j.LOGGER.info("New shipment created");
        } catch (Exception e) {
            //Log4j.LOGGER.info("Error al insertar revisar gramatica Json " + e.getMessage());
            return "Error al insertar revisar gramatica Json " + e.getMessage();
        }
    }    
    
    @POST
    @Path("assign-cadet")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    public String assignCadetToShipment(String jsonData) {
        try {
            Gson jsonParser = new Gson();
            JsonParser parser = new JsonParser();
            JsonObject data = parser.parse(jsonData).getAsJsonObject();
            
            int cadetId = jsonParser.fromJson(data.get("cadet"), int.class);
            Long shipmentId = jsonParser.fromJson(data.get("shipment"), Long.class);
            
            CadetsRestClient restClient = new CadetsRestClient();
            String jsonDataVehicle = restClient.getCadetInfo(String.valueOf(cadetId));
            
            JsonObject dataVehicle = parser.parse(jsonDataVehicle).getAsJsonObject();
            int vehicleId = jsonParser.fromJson(dataVehicle.get("vehicle"), int.class);
            String cadetEmail = jsonParser.fromJson(dataVehicle.get("email"), String.class);
            String cadetName = jsonParser.fromJson(dataVehicle.get("fullname"), String.class);
            
            shipmentService.assignSelectedCadet(shipmentId, cadetId, vehicleId, cadetEmail, cadetName);

            //usuarioBean.agregarUsuario(nuevoCadete);
            //Log4j.LOGGER.info("Cadete Agregado");
            return "Cadete asignado correctamente";
        } catch (Exception e) {
            //Log4j.LOGGER.info("Error al insertar revisar gramatica Json " + e.getMessage());
            return "Error al insertar revisar gramatica Json " + e.getMessage();
        }
    }
    
    @GET
    @Path("/mark-shipment-delivered")
    @Produces("text/plain")
    public String markShipmentAsDelivered(@QueryParam("shipmentId") Long shipmentId) {
        Gson jsonParser = new Gson();
        JsonParser parser = new JsonParser();
        
        Shipment currentShipment = shipmentService.getShipment(shipmentId);
        
        ClientsRestClient clientsRestClient = new ClientsRestClient();
        String jsonDataClientFrom = clientsRestClient.getClientInfo(String.valueOf(currentShipment.getClientFromId()));
        String jsonDataClientTo = clientsRestClient.getClientInfo(String.valueOf(currentShipment.getClientToId()));
        JsonObject dataClientFrom = parser.parse(jsonDataClientFrom).getAsJsonObject();
        JsonObject dataClientTo = parser.parse(jsonDataClientTo).getAsJsonObject();
        
        String clientFromEmail = jsonParser.fromJson(dataClientFrom.get("email"), String.class);
        String clientFromName = jsonParser.fromJson(dataClientFrom.get("fullname"), String.class);
        String clientToEmail = jsonParser.fromJson(dataClientTo.get("email"), String.class);
        String clientToName = jsonParser.fromJson(dataClientTo.get("fullname"), String.class);
        
        shipmentService.markShipmentAsDelivered(currentShipment, clientFromName, clientFromEmail, clientToName, clientToEmail);
        
        return "OK";
    }
    
    @GET
    @Path("/get-shipment-participants")
    @Produces(MediaType.APPLICATION_JSON)
    public String getShipmentParticipants(@QueryParam("shipmentId") Long shipmentId) {
        Shipment currentShipment = shipmentService.getShipment(shipmentId);
        
        return "{ clientFromId: " + currentShipment.getClientFromId() + ", "
               + "clientToId: \"" + currentShipment.getClientToId() + "\","
               + "cadetId: \"" + currentShipment.getCadetId() + "\" }";
    }    
}