/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviosya.shipments.frontend.restclients;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

/**
 * Jersey REST client generated for REST resource:CadetResource [cadet]<br>
 * USAGE:
 * <pre>
 *        CadetsRestClient client = new CadetsRestClient();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author ernestou
 */
public class CadetsRestClient {

    private static final String BASE_URI = "http://localhost:8080/Cadets-war";
    private WebTarget webTarget;
    private Client client;

    public CadetsRestClient() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("cadet");
    }

    public String getCadetInfo(String cadetId) throws ClientErrorException {
        WebTarget resource = webTarget;
        if (cadetId != null) {
            resource = resource.queryParam("cadetId", cadetId);
        }
        resource = resource.path("get-cadet-info");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(String.class);
    }

    public String getCloseCadets() throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("get-close-cadets");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(String.class);
    }

    public String createCadet(Object requestEntity) throws ClientErrorException {
        return webTarget.path("create-cadet").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), String.class);
    }

    public String addVehicle(Object requestEntity) throws ClientErrorException {
        return webTarget.path("add-vehicle").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), String.class);
    }

    public void close() {
        client.close();
    }
    
}
