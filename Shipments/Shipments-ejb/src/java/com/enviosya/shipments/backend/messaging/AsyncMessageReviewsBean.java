package com.enviosya.shipments.backend.messaging;

import com.enviosya.entitydto.dtos.ClientFromShipmentDto;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.ObjectMessage;

@Stateless(name = "AsyncMessageReviews")
public class AsyncMessageReviewsBean implements AsyncMessageBeanLocal {

    @Resource(lookup = "jms/ConnectionFactory")
    private ConnectionFactory cf;
    
    @Resource(lookup = "jms/QueueReviews")
    private Queue queueReviews;    
    
    public void enqueue(Object message) {
        try {
            Connection connection = cf.createConnection();
            Session session = connection.createSession();
            ObjectMessage objectMessage = session.createObjectMessage();
            objectMessage.setObject((ClientFromShipmentDto) message);
            
            connection.start();
            
            MessageProducer producer = session.createProducer(this.queueReviews);
            producer.send(objectMessage);
            
            session.close();
            connection.close();
        } catch (JMSException ex) {
            System.out.println(ex.getMessage());
            //Logger.getLogger(UsuarioBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
