package com.enviosya.shipments.backend.dataaccess;

import com.enviosya.shipments.backend.entity.Shipment;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ShipmentDataAccessBean extends AbstractFacade<Shipment> implements ShipmentDataAccessBeanLocal {

    @PersistenceContext(unitName = "Shipments-ejbPU")
    private EntityManager em;
    
    public ShipmentDataAccessBean() {
        super(Shipment.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }  
    
    @Override
    public boolean shipmentExists(Long id) {
        return find(id) != null;
    }
    
    @Override
    public Shipment getShipment(Long id) {
        Shipment shipment = find(id);
        if (shipment == null) { 
            //throw new NotFoundException(); 
        }
        return shipment;
    }

    @Override
    public void createShipment(Shipment shipment) {
        create(shipment);
    }
    
    @Override
    public void editShipment(Shipment shipment) {
        edit(shipment);
    }
}
