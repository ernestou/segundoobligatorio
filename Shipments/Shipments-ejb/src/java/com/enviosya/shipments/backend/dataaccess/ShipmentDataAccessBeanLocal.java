package com.enviosya.shipments.backend.dataaccess;

import com.enviosya.shipments.backend.entity.Shipment;

import javax.ejb.Local;

@Local
public interface ShipmentDataAccessBeanLocal {
    void createShipment(Shipment shipment); //throws Exception;
    
    Shipment getShipment(Long id); //throws Exception;

    void editShipment(Shipment shipment); //throws Exception;
   
    boolean shipmentExists(Long id);
}
