package com.enviosya.shipments.backend.operations;

import com.enviosya.shipments.backend.entity.Shipment;
import javax.ejb.Local;

/**
 *
 * @author ernestou
 */
@Local
public interface ShipmentBeanLocal {

    void createShipment(Shipment shipment);

    Shipment getShipment(Long id);

    void assignSelectedCadet(Long shipmentId, int cadetId, int vehicleId, String cadetEmail, String cadetName);

    void markShipmentAsDelivered(Shipment currentShipment, String clientFromName, String clientFromEmail, String clientToName, String clientToEmail);
}
