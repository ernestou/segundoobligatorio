package com.enviosya.shipments.backend.messaging;

import javax.ejb.Local;

@Local
public interface AsyncMessageBeanLocal {
    void enqueue(Object message);
}