package com.enviosya.shipments.backend.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "shipment")
public class Shipment {
     
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    private String description;
    
    private int clientFromId;
    
    private int clientToId;
    
    private int cadetId;
 
    private int vehicleId;
    
    private float cost;
    
    private String addressFrom;
    
    private String addressTo;
    
    private String clientFromName;
    
    private String clientToName;
    
    private String status;
    
    public Shipment() {
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getClientFromId() {
        return clientFromId;
    }

    public void setClientFromId(int clientFromId) {
        this.clientFromId = clientFromId;
    }

    public int getClientToId() {
        return clientToId;
    }

    public void setClientToId(int clientToId) {
        this.clientToId = clientToId;
    }

    public int getCadetId() {
        return cadetId;
    }

    public void setCadetId(int cadetId) {
        this.cadetId = cadetId;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }
    
    public String getAddressFrom() {
        return addressFrom;
    }

    public void setAddressFrom(String addressFrom) {
        this.addressFrom = addressFrom;
    }

    public String getAddressTo() {
        return addressTo;
    }

    public void setAddressto(String addressTo) {
        this.addressTo = addressTo;
    }

    public String getClientFromName() {
        return clientFromName;
    }

    public void setClientFromName(String clientFromName) {
        this.clientFromName = clientFromName;
    }

    public String getClientToName() {
        return clientToName;
    }

    public void setClientToName(String clientToName) {
        this.clientToName = clientToName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += id != null ? id.hashCode() : 0;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Shipment)) {
            return false;
        }
        Shipment other = (Shipment) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Envio[ id=" + id + " ]";
    }
}
