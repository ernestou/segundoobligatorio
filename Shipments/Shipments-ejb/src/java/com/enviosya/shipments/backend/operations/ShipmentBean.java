package com.enviosya.shipments.backend.operations;

import com.enviosya.shipments.backend.dataaccess.ShipmentDataAccessBeanLocal;
import com.enviosya.shipments.backend.entity.Shipment;
import com.enviosya.shipments.backend.messaging.AsyncMessageBeanLocal;
import com.enviosya.entitydto.dtos.ClientFromShipmentDto;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.enviosya.entitydto.dtos.ShipmentDto;

/**
 *
 * @author ernestou
 */
@Stateless
public class ShipmentBean implements ShipmentBeanLocal {

    @EJB
    private ShipmentDataAccessBeanLocal shipmentFacade;
    
    @EJB(beanName = "AsyncMessageCadets")
    private AsyncMessageBeanLocal asyncMessageCadetsService;
    
    @EJB(beanName = "AsyncMessageReviews")
    private AsyncMessageBeanLocal asyncMessageReviewsService;    
    
    @Override
    public void createShipment(Shipment shipment) {
        shipmentFacade.createShipment(shipment);
    }

    @Override
    public Shipment getShipment(Long id) {
        if (!shipmentFacade.shipmentExists(id)) {
            //THROW EXCEPTION
            System.out.println("NO EXISTE");
        }
        
        return shipmentFacade.getShipment(id);
    }

    @Override
    public void assignSelectedCadet(Long shipmentId, int cadetId, int vehicleId, String cadetEmail, String cadetName) {
        if (!shipmentFacade.shipmentExists(shipmentId)) {
            System.out.println("NO EXISTE");
        }
        Shipment currentShipment = shipmentFacade.getShipment(shipmentId);
        currentShipment.setCadetId(cadetId);
        currentShipment.setVehicleId(vehicleId);
        shipmentFacade.editShipment(currentShipment);
        
        ShipmentDto shipmentDto = new ShipmentDto();
        shipmentDto.setAddressFrom(currentShipment.getAddressFrom());
        shipmentDto.setAddressTo(currentShipment.getAddressTo());
        shipmentDto.setDescription(currentShipment.getDescription());
        
        shipmentDto.setClientNameFrom(currentShipment.getClientFromName());
        shipmentDto.setClientNameTo(currentShipment.getClientToName());
        shipmentDto.setEmailCadet(cadetEmail);
        shipmentDto.setNameCadet(cadetName);
        
        asyncMessageCadetsService.enqueue(shipmentDto);
    }

    @Override
    public void markShipmentAsDelivered(Shipment currentShipment, String clientFromName, String clientFromEmail, 
            String clientToName, String clientToEmail) {
        int shipId = Integer.valueOf(currentShipment.getId().toString());
        
        ClientFromShipmentDto clientFromDto = new ClientFromShipmentDto(clientFromName, clientFromEmail, shipId, true);
        ClientFromShipmentDto clientToDto = new ClientFromShipmentDto(clientToName, clientToEmail, shipId, false);
        
        asyncMessageReviewsService.enqueue(clientFromDto);
        asyncMessageReviewsService.enqueue(clientToDto);
        
        currentShipment.setStatus("Delivered");
        shipmentFacade.editShipment(currentShipment);
    }
    
    
}
