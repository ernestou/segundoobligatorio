package com.enviosya.shipments.backend.messaging;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import com.enviosya.entitydto.dtos.ShipmentDto;
import javax.jms.ObjectMessage;

@Stateless(name = "AsyncMessageCadets")
public class AsyncMessageCadetsBean implements AsyncMessageBeanLocal {

    @Resource(lookup = "jms/ConnectionFactory")
    private ConnectionFactory cf;
    
    @Resource(lookup = "jms/Queue")
    private Queue queueCadets; 
    
    public void enqueue(Object message) {
        try {
            Connection connection = cf.createConnection();
            Session session = connection.createSession();
            ObjectMessage objectMessage = session.createObjectMessage();
            objectMessage.setObject((ShipmentDto) message);
            
            connection.start();
            
            MessageProducer producer = session.createProducer(this.queueCadets);
            producer.send(objectMessage);
            
            session.close();
            connection.close();
        } catch (JMSException ex) {
            System.out.println(ex.getMessage());
            //Logger.getLogger(UsuarioBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
