package com.enviosya.mocknatlang.restclients;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NatlangRestClient implements NatlangRestClientInterface {

    public NatlangRestClient() {
    }

    @Override
    public String getFeelingNatlang(String comment) {
        try {
            TimeUnit.SECONDS.sleep(1);
            
            if (comment.contains("positivo")) {
                return "Positivo";
            } else if (comment.contains("negativo")) {
                return "Negativo";
            } else {
                return "Neutral";
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(NatlangRestClient.class.getName()).log(Level.SEVERE, null, ex);
            return "Neutral";
        }
    }        
}
