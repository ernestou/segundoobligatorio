package com.enviosya.entitydto.dtos;

import java.io.Serializable;

public class ShipmentDto implements Serializable {
    
    private String emailCadet;
    
    private String nameCadet;
    
    private String addressFrom;
    
    private String addressTo;
    
    private String description;
    
    private String clientNameFrom;
    
    private String clientNameTo;

    public ShipmentDto() {
    }

    public String getEmailCadet() {
        return emailCadet;
    }

    public void setEmailCadet(String emailCadet) {
        this.emailCadet = emailCadet;
    }

    public String getNameCadet() {
        return nameCadet;
    }

    public void setNameCadet(String nameCadet) {
        this.nameCadet = nameCadet;
    }

    public String getAddressFrom() {
        return addressFrom;
    }

    public void setAddressFrom(String addressFrom) {
        this.addressFrom = addressFrom;
    }

    public String getAddressTo() {
        return addressTo;
    }

    public void setAddressTo(String addressTo) {
        this.addressTo = addressTo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClientNameFrom() {
        return clientNameFrom;
    }

    public void setClientNameFrom(String clientNameFrom) {
        this.clientNameFrom = clientNameFrom;
    }

    public String getClientNameTo() {
        return clientNameTo;
    }

    public void setClientNameTo(String clientNameTo) {
        this.clientNameTo = clientNameTo;
    }
    
}
