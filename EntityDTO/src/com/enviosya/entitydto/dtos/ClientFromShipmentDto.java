package com.enviosya.entitydto.dtos;

import java.io.Serializable;

public class ClientFromShipmentDto implements Serializable {
    
    private String name;
    
    private String email;
    
    private int shipmentId;
    
    private boolean sender;

    public ClientFromShipmentDto() {
    }    
    
    public ClientFromShipmentDto(String name, String email, int shipmentId, boolean sender) {
        this.name = name;
        this.email = email;
        this.shipmentId = shipmentId;
        this.sender = sender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(int shipmentId) {
        this.shipmentId = shipmentId;
    }

    public boolean isSender() {
        return sender;
    }

    public void setSender(boolean sender) {
        this.sender = sender;
    }
}
