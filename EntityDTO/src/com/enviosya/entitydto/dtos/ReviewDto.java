package com.enviosya.entitydto.dtos;

import java.io.Serializable;

public class ReviewDto implements Serializable {
    private String comment;
    
    private Long reviewId;

    public ReviewDto() {
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getReviewId() {
        return reviewId;
    }

    public void setReviewId(Long reviewId) {
        this.reviewId = reviewId;
    }
}
