package com.enviosya.costcalculator.operations;

public interface CostCalculatorInterface {
    float obtainCost(String image);
}
