package com.enviosya.costcalculator.operations;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;


public class CostCalculator implements CostCalculatorInterface {

    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "https://ort-arqsoftort-sizer.herokuapp.com";

    public CostCalculator() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI);
    }
    
    @Override
    public float obtainCost(String image) {
        Gson jsonParser = new Gson();
        JsonParser parser = new JsonParser();
        
        String body = "{ \"image\": \"" + image + "\" }";
        
        System.err.println(body);
        String jsonResponse = 
            webTarget.path("dimensions").request()
            .header("Content-Type", "application/json")
            .header("Accept", "application/json")
            .post(javax.ws.rs.client.Entity.entity(body, javax.ws.rs.core.MediaType.APPLICATION_JSON), String.class);
        
        JsonObject dataShipment = parser.parse(jsonResponse).getAsJsonObject();

        float height = jsonParser.fromJson(dataShipment.get("height"), float.class);
        float length = jsonParser.fromJson(dataShipment.get("length"), float.class);
        float weight = jsonParser.fromJson(dataShipment.get("weight"), float.class);
        
        this.close();
        
        return height + length + weight;
    }
    
    public void close() {
        client.close();
    }        
}
