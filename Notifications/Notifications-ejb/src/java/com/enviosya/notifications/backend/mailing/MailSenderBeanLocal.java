package com.enviosya.notifications.backend.mailing;

import com.enviosya.notifications.backend.entity.Destination;
import com.enviosya.notifications.backend.entity.MessageNotification;
import javax.ejb.Local;

@Local
public interface MailSenderBeanLocal {

    void sendEmail(MessageNotification msg, Destination dest);
    
}
