/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviosya.notifications.backend.mailing;

import com.enviosya.notifications.backend.entity.Destination;
import com.enviosya.notifications.backend.entity.MessageNotification;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author ernestou
 */
@Stateless
public class MailSenderBean implements MailSenderBeanLocal {

    @Resource(lookup = "mail/gmailSession")
    private Session mailSession;    
    
    public void sendEmail(MessageNotification msg, Destination dest) {
        MimeMessage message = new MimeMessage(mailSession);
        try {
            message.setFrom(new InternetAddress("enviosya2017gr@gmail.com"));
            
            //InternetAddress[] address = {new InternetAddress("ernestou88@gmail.com")};
            
            ArrayList<InternetAddress> recipients = dest.getRecipients();
            InternetAddress[] addresses = new InternetAddress[recipients.size()];
            
            int auxIdx = 0;
            for (Iterator<InternetAddress> iterator = recipients.iterator(); iterator.hasNext();) {
                InternetAddress next = iterator.next();
                addresses[auxIdx] = new InternetAddress(next.getAddress());
                auxIdx++;
            }
            message.setRecipients(Message.RecipientType.TO, addresses);
            
            message.setSubject(msg.getSubject());
            message.setSentDate(new Date());
            message.setText(msg.getBody());
            Transport.send(message);
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }
    }
}
