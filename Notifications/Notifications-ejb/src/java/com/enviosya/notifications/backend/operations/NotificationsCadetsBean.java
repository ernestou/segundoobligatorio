package com.enviosya.notifications.backend.operations;

import com.enviosya.notifications.backend.mailing.MailSenderBeanLocal;
import com.enviosya.notifications.backend.entity.Destination;
import com.enviosya.notifications.backend.entity.MessageNotification;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import com.enviosya.entitydto.dtos.ShipmentDto;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.mail.internet.AddressException;

@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/Queue"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
    })

@Stateless
public class NotificationsCadetsBean implements MessageListener {

    //SESSION BEAN QUE ESCUCHA MAILS A CADETS
    @EJB
    private MailSenderBeanLocal mailService;
    
    @Override
    public void onMessage(Message message) {
        try {
            ObjectMessage objMessage = (ObjectMessage) message;
            
            ShipmentDto shipmentDto = (ShipmentDto) objMessage.getObject();
            
            Destination destination = new Destination();
            MessageNotification msg = new MessageNotification();
            
            destination.addRecipient(shipmentDto.getEmailCadet());
            msg.setSubject("Se le ha asignado un nuevo envío.");
            msg.setBody("HOLA HOLA HOLA HOLAAAAAA");
            
            System.out.println("----------ON MESSAGE----------");
            System.out.println(shipmentDto.getEmailCadet());
            
            mailService.sendEmail(msg, destination);
        } catch (JMSException ex) {
            System.out.println(ex.getMessage());
            //Logger.getLogger(EnvioMdBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (AddressException ex) {
            Logger.getLogger(NotificationsCadetsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
