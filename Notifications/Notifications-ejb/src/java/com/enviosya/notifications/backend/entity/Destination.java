package com.enviosya.notifications.backend.entity;

import java.util.ArrayList;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class Destination {
   
    private ArrayList<InternetAddress> recipients;

    public Destination() {
        recipients = new ArrayList<InternetAddress>();
    }

    public ArrayList<InternetAddress> getRecipients() {
        return recipients;
    }

    public void setRecipients(ArrayList<InternetAddress> recipients) {
        this.recipients = recipients;
    }
    
    public void addRecipient(String address) throws AddressException {
        InternetAddress recipient = new InternetAddress(address);
        this.recipients.add(recipient);
    }
    
}
