package com.enviosya.notifications.backend.operations;

import com.enviosya.notifications.backend.mailing.MailSenderBeanLocal;
import com.enviosya.notifications.backend.entity.Destination;
import com.enviosya.notifications.backend.entity.MessageNotification;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import com.enviosya.entitydto.dtos.ClientFromShipmentDto;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.mail.internet.AddressException;

@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/QueueReviews"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
    })

@Stateless
public class NotificationsReviewsBean implements MessageListener {

    @EJB
    private MailSenderBeanLocal mailService;
    
    @Override
    public void onMessage(Message message) {
        try {
            ObjectMessage objMessage = (ObjectMessage) message;
            
            ClientFromShipmentDto clientDto = (ClientFromShipmentDto) objMessage.getObject();
            
            Destination destination = new Destination();
            MessageNotification msg = new MessageNotification();
            
            destination.addRecipient(clientDto.getEmail());
            
            if (clientDto.isSender()) {
                msg.setSubject("Su envío ha llegado a destino");
                msg.setBody("Estimado " + clientDto.getName() + ": Su envío fue entregado satisfactoriamente.");
            } else {
                msg.setSubject("Ha recibido un paquete");
                msg.setBody("Estimado " + clientDto.getName() + ": Ha recibido un paquete.");
                //clientDto.getShipmentId();
            }
            
            System.out.println("----------ON MESSAGE----------");
            System.out.println(clientDto.getEmail());
            
            mailService.sendEmail(msg, destination);
        } catch (JMSException ex) {
            System.out.println(ex.getMessage());
            //Logger.getLogger(EnvioMdBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (AddressException ex) {
            Logger.getLogger(NotificationsReviewsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
