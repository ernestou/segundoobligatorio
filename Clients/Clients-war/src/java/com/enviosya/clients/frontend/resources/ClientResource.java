package com.enviosya.clients.frontend.resources;


import com.enviosya.clients.backend.entity.Client;
import com.enviosya.clients.backend.entity.PaymentMethod;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import com.enviosya.clients.backend.operations.ClientBeanLocal;
import com.enviosya.clients.backend.operations.PaymentMethodBeanLocal;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.security.MessageDigest;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author riccager
 */
@Path("client")
@RequestScoped
public class ClientResource {

    @Context
    private UriInfo context;
    
    @Inject
    private ClientBeanLocal clientService;
    
    @Inject
    private PaymentMethodBeanLocal paymentMethodService;    
    
    /**
     * Creates a new instance of ClientResource
     */
    public ClientResource() {
    }
    
    @POST
    @Path("create-client")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    public String createClient(String jsonData) {
        try {
            Gson jsonParser = new Gson();
            Client newClient = jsonParser.fromJson(jsonData, Client.class);
            clientService.createClient(newClient);
            //usuarioBean.agregarUsuario(nuevoCliente);
            //Log4j.LOGGER.info("Cliente Agregado");
            return "Cliente agregado correctamente.";
        } catch (Exception e) {
            //Log4j.LOGGER.info("Error al insertar revisar gramatica Json " + e.getMessage());
            return "Error al insertar revisar gramatica Json " + e.getMessage();
        }
    }

    @POST
    @Path("add-payment-method")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    public String addPaymentMethod(String jsonData) {
        try {
            Gson jsonParser = new Gson();
            JsonParser parser = new JsonParser();
            JsonObject data = parser.parse(jsonData).getAsJsonObject();
            
            Long clientId = jsonParser.fromJson(data.get("client"), Long.class);
            
            PaymentMethod newPaymentMethod = jsonParser.fromJson(data.get("payment_method"), PaymentMethod.class);
            
            Client currentclient = clientService.getClient(clientId);
            
            newPaymentMethod.setClient(currentclient);
            currentclient.getPaymentMethods().add(newPaymentMethod);

            MessageDigest md = MessageDigest.getInstance("SHA-256");
            
            byte[] bytesCardNumber = newPaymentMethod.getCardNumber().getBytes("UTF-8");
            byte[] digestCardNumber = md.digest(bytesCardNumber);
            byte[] bytesCvv = newPaymentMethod.getCvv().getBytes("UTF-8");
            byte[] digestCvv = md.digest(bytesCvv);
            
            newPaymentMethod.setCardNumber(String.valueOf(digestCardNumber));
            newPaymentMethod.setCvv(String.valueOf(digestCvv));
            
            paymentMethodService.createPaymentMethod(newPaymentMethod);
            clientService.editClient(currentclient);

            //Log4j.LOGGER.info("Cadete Agregado");
            return "Método de pago agregado correctamente.";
        } catch (Exception e) {
            //Log4j.LOGGER.info("Error al insertar revisar gramatica Json " + e.getMessage());
            return "Error al insertar revisar gramatica Json " + e.getMessage();
        }
    }
    
    @GET
    @Path("/get-client-info")
    @Produces(MediaType.APPLICATION_JSON)
    public String getClientInfo(@QueryParam("clientId") Long clientId) {
        Client currentClient = clientService.getClient(clientId);
        
        return "{ fullname: \"" + currentClient.getNombreCompleto() + "\", "
               + "email: \"" + currentClient.getEmail() + "\", "
               + "ci: \"" + currentClient.getCi() + "\" }";
        
    }
}