package com.enviosya.clients.backend.dataaccess;

import com.enviosya.clients.backend.entity.PaymentMethod;
//import backend.exceptions.DuplicateUserException;
//import backend.exceptions.UserNotFoundException;

import javax.ejb.Local;

@Local
public interface PaymentMethodDataAccessBeanLocal {
  
    void createPaymentMethod(PaymentMethod paymentMethod); //throws DuplicateUserException;
    
    PaymentMethod getPaymentMethod (Long id); //throws UserNotFoundException;
}
