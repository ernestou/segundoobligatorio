/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviosya.clients.backend.operations;

import com.enviosya.clients.backend.dataaccess.ClientDataAccessBeanLocal;
import com.enviosya.clients.backend.entity.Client;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author ernestou
 */
@Stateless
public class ClientBean implements ClientBeanLocal {

    @EJB
    private ClientDataAccessBeanLocal clientFacade;    
    
    @Override
    public void createClient(Client client) {
        clientFacade.createClient(client);
    }

    @Override
    public Client getClient(Long id) {
        return clientFacade.getClient(id);
    }

    @Override
    public void editClient(Client client) {
        clientFacade.editClient(client);
    }

    @Override
    public boolean clientExists(Long id) {
        return clientFacade.clientExists(id);
    }

}
