package com.enviosya.clients.backend.operations;

import com.enviosya.clients.backend.dataaccess.PaymentMethodDataAccessBeanLocal;
import com.enviosya.clients.backend.entity.PaymentMethod;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class PaymentMethodBean implements PaymentMethodBeanLocal {

    @EJB
    private PaymentMethodDataAccessBeanLocal paymentMethodFacade;
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    @Override
    public void createPaymentMethod(PaymentMethod paymentMethod) {
        paymentMethodFacade.createPaymentMethod(paymentMethod);
    }

    @Override
    public PaymentMethod getPaymentMethod(Long id) {
        return paymentMethodFacade.getPaymentMethod(id);
    }
}
