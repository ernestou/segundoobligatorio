package com.enviosya.clients.backend.dataaccess;

import com.enviosya.clients.backend.entity.PaymentMethod;
//import backend.exceptions.DuplicateVehicleException;
//import backend.exceptions.VehicleNotFoundException;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class PaymentMethodDataAccessBean extends AbstractFacade<PaymentMethod> implements PaymentMethodDataAccessBeanLocal {

    @PersistenceContext(unitName = "Clients-ejbPU")
    private EntityManager em;
    
    public PaymentMethodDataAccessBean() {
        super(PaymentMethod.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @Override
    public PaymentMethod getPaymentMethod(Long id) {
        PaymentMethod paymentMethod = find(id);
        if (paymentMethod == null) { 
            //throw new VehicleNotFoundException(); 
        }
        return paymentMethod;
    }

    @Override
    public void createPaymentMethod(PaymentMethod paymentMethod) {
        create(paymentMethod);
    }
    
   
}
