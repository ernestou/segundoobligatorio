package com.enviosya.clients.backend.operations;

import com.enviosya.clients.backend.entity.PaymentMethod;
import javax.ejb.Local;


@Local
public interface PaymentMethodBeanLocal {
    void createPaymentMethod(PaymentMethod paymentMethod); //throws DuplicateUserException;
    
    PaymentMethod getPaymentMethod (Long id); //throws UserNotFoundException;
}
