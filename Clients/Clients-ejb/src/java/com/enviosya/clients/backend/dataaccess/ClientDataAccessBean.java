package com.enviosya.clients.backend.dataaccess;

import com.enviosya.clients.backend.entity.Client;
//import backend.exceptions.DuplicateClientException;
//import backend.exceptions.ClientNotFoundException;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ClientDataAccessBean extends AbstractFacade<Client> implements ClientDataAccessBeanLocal {

    @PersistenceContext(unitName = "Clients-ejbPU")
    private EntityManager em;
    
    public ClientDataAccessBean() {
        super(Client.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    
    @Override
    public boolean clientExists(Long id) {
        return find(id) != null;
    }
    
    @Override
    public Client getClient(Long id) {
        Client client = find(id);
        if (client == null) { 
            //throw new ClientNotFoundException(); 
        }
        return client;
    }

    @Override
    public void createClient(Client client) {
        create(client);
    }
    
    @Override
    public void editClient(Client client) {
        edit(client);
    }
}
