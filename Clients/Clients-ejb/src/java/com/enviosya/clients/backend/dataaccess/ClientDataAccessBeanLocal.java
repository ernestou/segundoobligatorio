package com.enviosya.clients.backend.dataaccess;

import com.enviosya.clients.backend.entity.Client;
//import backend.exceptions.DuplicateUserException;
//import backend.exceptions.UserNotFoundException;

import javax.ejb.Local;

@Local
public interface ClientDataAccessBeanLocal {
    void createClient(Client client); //throws DuplicateUserException;
    
    Client getClient(Long id); //throws UserNotFoundException;

    void editClient(Client client); //throws UserNotFoundException;
   
    boolean clientExists(Long id);
}
