/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviosya.clients.backend.operations;

import com.enviosya.clients.backend.entity.Client;
import javax.ejb.Local;

/**
 *
 * @author ernestou
 */
@Local
public interface ClientBeanLocal {
    void createClient(Client client); //throws DuplicateUserException;
    
    Client getClient(Long id); //throws UserNotFoundException;

    void editClient(Client client); //throws UserNotFoundException;
   
    boolean clientExists(Long id);    
}
